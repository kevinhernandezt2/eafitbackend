usuario_login = {
    "data" : [{
        "id":1,
        "nombres": "kevin",
        "apellidos": "johan",
        "tipoIdentificacion": "12345",
        "correo": "jhernandezk@uninorte.edu.co",
        "telefono": "4444444",
        "rol": 1
        },
        {
        "id":2,
        "nombres": "kevin",
        "apellidos": "johan",
        "tipoIdentificacion": "12345",
        "correo": "kevin@uninorte.edu.co",
        "telefono": "4444444",
        "rol": 2
        },
        {
        "id":3,
        "nombres": "kevin",
        "apellidos": "johan",
        "tipoIdentificacion": "12345",
        "correo": "johan@uninorte.edu.co",
        "telefono": "4444444",
        "rol": 3
        }]
}

listar_usuarios_admin = [{
        "id": 1,
        "nombres": "kevin",
        "apellidos": "johan",
        "correo": "jhernandezk@uninorte.edu.co",
        "rol": "superadmin"
    },
    {   
        "id": 2,
        "nombres": "kevin",
        "apellidos": "johan",
        "correo": "kevin@uninorte.edu.co",
        "rol": "docente"
    },
    {   
        "id": 3,
        "nombres": "kevin",
        "apellidos": "johan",
        "correo": "johan@uninorte.edu.co",
        "rol": "estudiante"
    }
]

listar_materias_admin = [{
        "id": 1,
        "nombre": "matematicas",
        "descripcion": "fffffff"
    },
    {   
       "id": 2,
        "nombre": "biologia",
        "descripcion": "fffffff"
    },
    {   
        "id": 3,
        "nombre": "literatura",
        "descripcion": "fffffff"
    }
]

listar_asi_docentes_admin = [{
        "id": 1,
        "materia": "matematicas",
        "docente": "fulanito"
    },
    {   
       "id": 2,
        "materia": "biologia",
        "docente": "fulanito"
    },
    {   
        "id": 3,
        "materia": "literatura",
        "docente": "fulanito"
    }
]

listar_asi_estudiantes_admin = [{
        "id": 1,
        "materia": "matematicas",
        "estudiante": "kevin"
    },
    {   
       "id": 2,
        "materia": "biologia",
        "estudiante": "isaac"
    },
    {   
        "id": 3,
        "materia": "literatura",
        "estudiante": "jhon"
    }
]

listar_materias_docente = [
    {
        "id_materia_usuario": 1,
        "usuario": {
            "id": 2,
            "nombreUsuario":"kevin",
            "apellidoUsuario":"Hernandez",
            "correo":"kevin@uninorte.edu.co",
            "rol":2,
        },
        "materia": {
            "id": 1,
            "nombre": "matecho",
            "descripcion": "ddddd"
        }
    },
    {
        "id_materia_usuario": 2,
        "usuario": {
            "id": 2,
            "nombreUsuario":"kevin",
            "apellidoUsuario":"Hernandez",
            "correo":"kevin@uninorte.edu.co",
            "rol":2,
        },
        "materia":{
            "id": 2,
            "nombre": "biologia",
            "descripcion": "sssss"
        }
    },
    {
        "id_materia_usuario": 3,
        "usuario": {
            "id": 2,
            "nombreUsuario":"kevin",
            "apellidoUsuario":"Hernandez",
            "correo":"kevin@uninorte.edu.co",
            "rol":2,
        },
        "materia":{
            "id": 3,
            "nombre": "estadistica",
            "descripcion": "ssssss"
        }
    }
]

listar_actividades_docente = [
    {
        "idactividadmateria": 1,
        "materiaxusuariodocente": {
            "idmateriausuario": 1,
            "usuario": {
                "id": 2,
                "nombreusuario":"kevin",
                "apellidousuario":"Hernandez",
                "correo":"kevin@uninorte.edu.co",
                "rol":2,
            },
            "materia": {
                "id": 1,
                "nombre": "matecho",
                "descripcion": "ddddd"
            },
        },
        "materiaxusuarioestudiante": {
            "idmateriausuario": 1,
            "usuario": {
                "id": 1,
                "nombreusuario":"isaac",
                "apellidousuario":"",
                "correo":"isaac@uninorte.edu.co",
                "rol":3,
            },
            "materia": {
                "id": 1,
                "nombre": "matecho",
                "descripcion": "ddddd"
            },
        },
        "detalleactividad":"haga las ecuanciones matematicas"
    },
    {
        "idactividadmateria": 2,
        "materiaxusuariodocente": {
            "idmateriausuario": 1,
            "usuario": {
                "id": 2,
                "nombreusuario":"kevin",
                "apellidousuario":"Hernandez",
                "correo":"kevin@uninorte.edu.co",
                "rol":2,
            },
            "materia": {
                "id": 1,
                "nombre": "matecho",
                "descripcion": "ddddd"
            },
        },
        "materiaxusuarioestudiante": {
            "idmateriausuario": 2,
            "usuario": {
                "id": 2,
                "nombreusuario":"jhon",
                "apellidousuario":"",
                "correo":"jhon@uninorte.edu.co",
                "rol":3,
            },
            "materia": {
                "id": 1,
                "nombre": "matecho",
                "descripcion": "ddddd"
            },
        },
        "detalleactividad":"haga las ecuaciones matematicas"
    }
]

combo_estudiantes_actividad_materia = {
        "materiaxUsuarioEstudiante": [
            {
                "id": 1,
                "usuario": {
                    "id": 1,
                    "nombreUsuario":"isaac",
                    "apellidoUsuario":"",
                    "correo":"isaac@uninorte.edu.co",
                    "rol":3,
                },
                "materia": {
                    "id": 1,
                    "nombre": "matecho",
                    "descripcion": "ddddd"
                }
            },
            {
                "id": 2,
                "usuario": {
                    "id": 2,
                    "nombreUsuario":"jhon",
                    "apellidoUsuario":"vargas",
                    "correo":"johh@uninorte.edu.co",
                    "rol":3,
                },
                "materia": {
                    "id": 1,
                    "nombre": "matecho",
                    "descripcion": "ddddd"
                }
            },
            {
                "id": 3,
                "usuario": {
                    "id": 3,
                    "nombreUsuario":"carolina",
                    "apellidoUsuario":"",
                    "correo":"carolina@uninorte.edu.co",
                    "rol":3,
                },
                "materia": {
                    "id": 1,
                    "nombre": "matecho",
                    "descripcion": "ddddd"
                }
            },
            {
                "id": 4,
                "usuario": {
                    "id": 2,
                    "nombreUsuario":"lui",
                    "apellidoUsuario":"mendoza",
                    "correo":"luis@uninorte.edu.co",
                    "rol":3,
                },
                "materia": {
                    "id": 1,
                    "nombre": "matecho",
                    "descripcion": "ddddd"
                }
            }
        ]
           
}

combo_docente_actividad_materia = {

        "materiaxUsuarioDocente": {
            "id": 1,
            "usuario": {
                "id": 2,
                "nombreUsuario":"kevin",
                "apellidoUsuario":"Hernandez",
                "correo":"kevin@uninorte.edu.co",
                "rol":2,
            },
            "materia": {
                "id": 1,
                "nombre": "matecho",
                "descripcion": "ddddd"
            }
        }
}

listar_actividades_calificacion_docente = [
    {
        "id_actividad_materia": 1,
        "materiaxUsuarioDocente": {
            "id_materia_usuario": 1,
            "usuario": {
                "id": 2,
                "nombreUsuario":"kevin",
                "apellidoUsuario":"Hernandez",
                "correo":"kevin@uninorte.edu.co",
                "rol":2,
            },
            "materia": {
                "id": 1,
                "nombre": "matecho",
                "descripcion": "ddddd"
            },
        },
        "materiaxUsuarioEstudiante": {
            "id_materia_usuario": 1,
            "usuario": {
                "id": 1,
                "nombreUsuario":"isaac",
                "apellidoUsuario":"",
                "correo":"isaac@uninorte.edu.co",
                "rol":3,
            },
            "materia": {
                "id": 1,
                "nombre": "matecho",
                "descripcion": "ddddd"
            },
        },
        "nombreActividad":"matematicas ecuaciones",
        "calificacion":5.0
    },
    {
        "id_actividad_materia": 2,
        "materiaxUsuarioDocente": {
            "id_materia_usuario": 2,
            "usuario": {
                "id": 2,
                "nombreUsuario":"kevin",
                "apellidoUsuario":"Hernandez",
                "correo":"kevin@uninorte.edu.co",
                "rol":2,
            },
            "materia": {
                "id": 2,
                "nombre": "biologia",
                "descripcion": "bbbbbbbssss"
            },
        },
        "materiaxUsuarioEstudiante": {
            "id_materia_usuario": 3,
            "usuario": {
                "id": 2,
                "nombreUsuario":"jhon",
                "apellidoUsuario":"",
                "correo":"jhon@uninorte.edu.co",
                "rol":3,
            },
            "materia": {
                "id": 2,
                "nombre": "biologia",
                "descripcion": "bbbbbbbssss"
            },
        },
       "nombreActividad":"biologia anatomia",
        "calificacion":2.0
    },
    {
        "id_actividad_materia": 3,
        "materiaxUsuarioDocente": {
            "id_materia_usuario": 3,
            "usuario": {
                "id": 2,
                "nombreUsuario":"kevin",
                "apellidoUsuario":"Hernandez",
                "correo":"kevin@uninorte.edu.co",
                "rol":2,
            },
            "materia": {
                "id": 3,
                "nombre": "literatura",
                "descripcion": "ffffffffff"
            },
        },
        "materiaxUsuarioEstudiante": {
            "id_materia_usuario": 4,
            "usuario": {
                "id": 3,
                "nombreUsuario":"virgilio",
                "apellidoUsuario":"",
                "correo":"dilan@uninorte.edu.co",
                "rol":3,
            },
            "materia": {
                "id": 3,
                "nombre": "literatura",
                "descripcion": "gggggg"
            },
        },
       "nombreActividad":"literatura anatomia",
        "calificacion":3.0
    }
]