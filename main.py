from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin
import ValidationDataModel

app = Flask(__name__)

# api_v2_cors_config = {
#   "origins": ["*"],
#   "methods": ["GET", "POST", "PUT", "DELETE"],
#   "allow_headers": ["Content-Type"]
# }

# CORS(app, resources={"/*": api_v2_cors_config})

CORS(app, resources={r"/*": {"origins": "*"}})

validationDataModel =  ValidationDataModel

contextPath = {
    "login": "/login/",
    "usuarios": "/usuarios/",
    "materias": "/materias/",
    "asignaciones": "/asignaciones/",
    "calificaciones": "/calificaciones/",
}


@app.route(contextPath['login']+"/ingreso", methods=['POST'])
def login():
    keyPass = "1234567"

    request_login = request.get_json()
    email = request_login["email"]
    contrasena = request_login["password"]
    resultado = {}
    contador = 0
    for x in validationDataModel.usuario_login["data"]:

        if email == x["correo"] and contrasena == keyPass:
            resultado =  validationDataModel.usuario_login["data"][contador]
        contador +=1
    
    if len(resultado) == 0:
        resultado = {"message":"no es ni el usuario ni la contrasena"}
    
    
    return jsonify(resultado)

'''
    flujo super admin
'''


@app.route(contextPath['usuarios']+"/listar-usuarios", methods=['GET'])
def listar_usuarios():
   resultado = {}
   if len(validationDataModel.listar_usuarios_admin) > 0:
        resultado = validationDataModel.listar_usuarios_admin
   else:
       resultado = {"message":"no hay usuarios registrados"}

   return jsonify(resultado)

@app.route(contextPath['usuarios']+"/crear-usuario", methods=['POST'])
def crear_usuario():

    resultado = {}

    request_crear_usuario = request.get_json()
    nombres = request_crear_usuario["nombres"]
    apellidos = request_crear_usuario["apellidos"]
    tipoIdentificacion = request_crear_usuario["tipoIdentificacion"]
    identificacion = request_crear_usuario["identificacion"]
    correo = request_crear_usuario["correo"]
    celular = request_crear_usuario["celular"]
    rol = request_crear_usuario["rol"]
    contrasena = request_crear_usuario["contrasena"]

    if not identificacion == "" and not correo == "" and not rol == "" and not contrasena == "":
        resultado = {"message": "creacion exitosa"}
    else:
        resultado = {"message": "creacion fallida"}

    return jsonify(resultado)

@app.route(contextPath['usuarios']+"/editar-usuario/<int:id_usuario>", methods=['PUT'])
def editar_usuario(id_usuario):

    resultado = {}

    if id_usuario != None:

        request_crear_usuario = request.get_json()
        nombres = request_crear_usuario["nombres"]
        apellidos = request_crear_usuario["apellidos"]
        tipoIdentificacion = request_crear_usuario["tipoIdentificacion"]
        identificacion = request_crear_usuario["identificacion"]
        correo = request_crear_usuario["correo"]
        celular = request_crear_usuario["celular"]
        rol = request_crear_usuario["rol"]

        if not identificacion == "" and not correo == "" and not rol == "":
            resultado = {"message": "edicion exitosa"}
        else:
            resultado = {"message": "edicion fallida"}

        return jsonify(resultado)
    
    else:

        resultado = {"message": "ha surgido un error en la edicion"}
        return jsonify(resultado)

@app.route(contextPath['usuarios']+"/eliminar-usuario/<int:id_usuario>", methods=['DELETE'])
def eliminar_usuario(id_usuario):

    resultado = {}

    if id_usuario != None:
        resultado = {"message": "eliminacion exitosa"}
        return jsonify(resultado)
    
    else:
        resultado = {"message": "eliminacion fallida"}
        return jsonify(resultado)



@app.route(contextPath['materias']+"/listar-materias", methods=['GET'])
def listar_materias():
   resultado = {}
   if len(validationDataModel.listar_materias_admin) > 0:
        resultado = validationDataModel.listar_materias_admin
   else:
       resultado = {"message":"no hay materias registrados"}

   return jsonify(resultado)

@app.route(contextPath['materias']+"/crear-materia", methods=['POST'])
def crear_materia():

    resultado = {}

    request_crear_materia = request.get_json()
    nombre = request_crear_materia["nombre"]
    descripcion = request_crear_materia["descripcion"]

    if not nombre == "":
        resultado = {"message": "creacion exitosa"}
    else:
        resultado = {"message": "creacion fallida"}

    return jsonify(resultado)

@app.route(contextPath['materias']+"/editar-materia/<int:id_materia>", methods=['PUT'])
def editar_materia(id_materia):

    resultado = {}

    if id_materia != None:

        request_crear_materia = request.get_json()
        nombre = request_crear_materia["nombre"]
        descripcion = request_crear_materia["descripcion"]

        if  not nombre == "":
            resultado = {"message": "edicion exitosa"}
        else:
            resultado = {"message": "edicion fallida"}

        return jsonify(resultado)
    
    else:

        resultado = {"message": "ha surgido un error en la edicion"}
        return jsonify(resultado)

@app.route(contextPath['materias']+"/eliminar-materia/<int:id_materia>", methods=['DELETE'])
def eliminar_materia(id_materia):

    resultado = {}

    if id_materia != None:
        resultado = {"message": "eliminacion exitosa"}
        return jsonify(resultado)
    
    else:
        resultado = {"message": "eliminacion fallida"}
        return jsonify(resultado)



@app.route(contextPath['asignaciones']+"/listar-asi_docentes", methods=['GET'])
def listar_asi_docentes():
   resultado = {}
   if len(validationDataModel.listar_asi_docentes_admin) > 0:
        resultado = validationDataModel.listar_asi_docentes_admin
   else:
       resultado = {"message":"no hay asignaciones registrados"}

   return jsonify(resultado)

@app.route(contextPath['asignaciones']+"/crear-asi_docentes", methods=['POST'])
def crear_asi_docentes():

    resultado = {}

    request_body = request.get_json()
    listado_materias = request_body["materias"]
    listado_docentes = request_body["docentes"]

    if len(listado_materias) > 0 and len(listado_docentes) > 0:
        resultado = {"message": "asignacion exitosa"}
    else:
        resultado = {"message": "asignacion fallida"}

    return jsonify(resultado)



@app.route(contextPath['asignaciones']+"/listar-asi_estudiantes", methods=['GET'])
def listar_asi_estudiantes():
   resultado = {}
   if len(validationDataModel.listar_asi_estudiantes_admin) > 0:
        resultado = validationDataModel.listar_asi_estudiantes_admin
   else:
       resultado = {"message":"no hay asignaciones registrados"}

   return jsonify(resultado)

@app.route(contextPath['asignaciones']+"/crear-asi_estudiantes", methods=['POST'])
def crear_asi_estudiantes():

    resultado = {}

    request_body = request.get_json()
    listado_materias = request_body["materias"]
    listado_estudiantes = request_body["estudiantes"]

    if len(listado_materias) > 0 and len(listado_estudiantes) > 0:
        resultado = {"message": "asignacion exitosa"}
    else:
        resultado = {"message": "asignacion fallida"}

    return jsonify(resultado)


'''
    flujo docente
'''


@app.route(contextPath['materias']+"/registro-actividades/listar-materias-docente", methods=['GET'])
def listar_materias_ra_docente():
   resultado = {}
   if len(validationDataModel.listar_materias_docente) > 0:
        resultado = validationDataModel.listar_materias_docente
   else:
       resultado = {"message":"no hay materias registradas"}

   return jsonify(resultado)


@app.route(contextPath['materias']+"/registro-actividades/listar-actividades-docente", methods=['GET'])
def listar_actividades_ra_docente():
   resultado = {}
   if len(validationDataModel.listar_actividades_docente) > 0:
        resultado = validationDataModel.listar_actividades_docente
   else:
       resultado = {"message":"no hay actividades registradas"}

   return jsonify(resultado)


@app.route(contextPath['materias']+"/registro-actividades/combo_estudiantes_actividad_materia", methods=['GET'])
def llena_combo_estudiantes_actividad_materia():
   resultado = {}
   resultado = validationDataModel.combo_estudiantes_actividad_materia
   return jsonify(resultado)

@app.route(contextPath['materias']+"/registro-actividades/combo_docente_actividad_materia", methods=['GET'])
def llena_combo_docente_actividad_materia():
   resultado = {}
   resultado = validationDataModel.combo_docente_actividad_materia
   return jsonify(resultado)


@app.route(contextPath['materias']+"/registro-actividades/crear-actividad", methods=['POST'])
def crear_actividad_docente():

    resultado = {}

    request_body = request.get_json()
    materiaxUsuarioDocente = request_body["materiaxUsuarioDocente"]
    materiaxUsuarioEstudiante = request_body["materiaxUsuarioEstudiante"]
    nombreActividad = request_body["nombreActividad"]
    detalleActividad = request_body["detalleActividad"]
    # calificacion = request_body["calificacion"]
    # detalleRetroalimentacion = request_body["detalleRetroalimentacion"]


    if materiaxUsuarioDocente > 0 and len(materiaxUsuarioEstudiante) > 0 and  not nombreActividad == "" and not detalleActividad == "":
        resultado = {"message": "actividad se creo exitosa"}
    else:
        resultado = {"message": "actividad fallo al guardar"}

    return jsonify(resultado)


@app.route(contextPath['calificaciones']+"/gestion-calificaciones/listar-calificaciones", methods=['GET'])
def listar_actividades_gc_docente():
   resultado = {}
   if len(validationDataModel.listar_actividades_calificacion_docente) > 0:
        resultado = validationDataModel.listar_actividades_calificacion_docente
   else:
       resultado = {"message":"no hay actividades aun gestionadas para calificar"}

   return jsonify(resultado)

@app.route(contextPath['calificaciones']+"/gestion-calificaciones/update-calificacion/<int:id_actividad_materia>", methods=['PUT'])
def update_actividad_calificacion(id_actividad_materia = None):

    resultado = {}
    if id_actividad_materia != None:
       

        request_body = request.get_json()
        materiaxUsuarioDocente = request_body["materiaxUsuarioDocente"]
        materiaxUsuarioEstudiante = request_body["materiaxUsuarioEstudiante"]
        nombreActividad = request_body["nombreActividad"]
        detalleActividad = request_body["detalleActividad"]
        calificacion = request_body["calificacion"]
        detalleRetroalimentacion = request_body["detalleRetroalimentacion"]


        if materiaxUsuarioDocente > 0 and materiaxUsuarioEstudiante > 0 and  not nombreActividad == "" and not detalleActividad == "" and calificacion >= 0 and detalleRetroalimentacion != "":
            resultado = {"message": "calificacion se gestiono exitosamente"}
        else:
            resultado = {"message": "fallo al momento de la calificacion"}

        return jsonify(resultado)
    else:
        resultado = {"message": "fallo, espere un momento y vuelva a validar"}


     

if __name__ == '__main__':
    app.run(debug=True)